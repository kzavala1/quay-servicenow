import csv,os
import requests
from datetime import datetime
import pandas as pd

def get_vals():
    ruta_archivo = "/home/ugsop_ocp/quayservicenow/app_info.csv"

    with open(ruta_archivo) as archivo_csv:
        lector_csv = csv.reader(archivo_csv, delimiter=',')
        lista_valores = []
        for fila in lector_csv:
            lista_valores.append(fila)

    return lista_valores

def get_shas(lista_valores):
    
    access_pass = os.environ['token']
    url_quay = os.environ['url_accs']
    vuln_inf = []
    headers = {"Authorization": f"Bearer {access_pass}"}
    with open('salida.csv', mode='w', newline='') as csv_file:
         fieldnames = ['sha','cod_vul', 'Severity', 'FixedBy', 'Link', 'Description', 'Score']
         writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
         writer.writeheader()
         for valor in lista_valores[1:]:
             nombre_org = valor[3]
             nombre_repo = valor[4]
             sha = valor[2]
             url_base = f"{url_quay}/api/v1/repository/{nombre_org}/{nombre_repo}/manifest/{sha}/security?vulnerabilities=true"
             try:
                 response = requests.get(url_base, headers=headers, timeout=10, verify=False)
                 if response.status_code == 200:
                    data = response.json()
                    for feature in data['data']['Layer']['Features']:
                        if feature.get("Vulnerabilities"):
                           for vuln_info in feature["Vulnerabilities"]:
                               Description = vuln_info.get("Description", "")
                               Description = Description.replace("\n", " ")
                               Description = Description.replace(",", "")
                               if "pyup" in vuln_info["Name"]:
                                   print(vuln_info["Name"])
                               else:
                                    writer.writerow({ 'sha': sha,
                                                     'cod_vul': vuln_info["Name"].replace('\n', '.'),
                                                     'Severity': vuln_info["Severity"].replace('\n', '.'),
                                                     'FixedBy': vuln_info["FixedBy"].replace('\n', '.'),
                                                     'Link': vuln_info["Link"].replace('\n', '.'),
                                                     'Description': Description,
                                                     'Score': vuln_info["Metadata"]["NVD"]["CVSSv3"]["Score"]})
                 else:
                     print("Error de get:",response.text)
             except Exception as e:
                 print(f"{e}")
    print("Archivo CSV creado exitosamente!")

lista_valores = get_vals()
get_shas(lista_valores)

df = pd.read_csv('app_info.csv')
df['ult_mod'] = pd.to_datetime(df['ult_mod']).dt.strftime("%d/%m/%Y")
df.to_csv('app_info.csv', index=False)
df1 = pd.read_csv('salida.csv')
df2 = pd.read_csv('app_info.csv')
merged_df = pd.merge(df1, df2, on='sha')
merged_df = merged_df.drop('sha', axis=1)
merged_df.to_csv('merged_info.csv', index=False)

from itertools import tee, islice, chain

with open('merged_info.csv', 'r') as f_in, open('archivo_sin_saltos.csv', 'w') as f_out:   
  def previous_and_next(some_iterable):
      prevs, items, nexts = tee(some_iterable, 3)
      prevs = chain([None], prevs)
      nexts = chain(islice(nexts, 1, None), [None])
      return zip(prevs, items, nexts)

  for previous, item, nxt in previous_and_next(f_in):
      if ("pyup" in item and "https://pyup" in nxt) or ("pyup" in item and "https://git" in nxt):
        #Elimina ultimo elemento de item
        lista = item.strip()
        arrayLista = lista.split(',')
        cantEnLista = len(arrayLista)
        itemFinal = ','.join(map(str,arrayLista[0:cantEnLista-1]))
        ultPosicionItem = ''.join(map(str,arrayLista[cantEnLista-1]))

        line_sin_saltos = itemFinal + "," + ultPosicionItem + " " + nxt
        f_out.write(line_sin_saltos)    
      else:
        if ("https://pyup" in item) or ("https://git" in item):
          print("no anexar linea")
        else:
          line_sin_saltos = item.strip() + '\n'
          f_out.write(line_sin_saltos)        